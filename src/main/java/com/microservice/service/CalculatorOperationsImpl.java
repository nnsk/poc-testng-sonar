package com.microservice.service;

import org.springframework.stereotype.Service;

import com.microservice.model.Operando;

@Service
public class CalculatorOperationsImpl implements ICalculatorOperations{

	@Override
	public Double sumar(Operando operandos) {
		// TODO Auto-generated method stub
		return operandos.getValor1() + operandos.getValor2();
	}

	@Override
	public int sumarValoresPositivos(int... numbers) {
		// TODO Auto-generated method stub
		int sum = 0;
        for (int number : numbers) {
            if(number>0){
                sum += number;
            }
        }
        return sum;
	}

	@Override
	public Double restar(Operando operandos) {
		// TODO Auto-generated method stub
		return operandos.getValor1() - operandos.getValor2();
	}

	@Override
	public Double multiplicar(Operando operandos) {
		// TODO Auto-generated method stub
		return operandos.getValor1() * operandos.getValor2();
	}

	@Override
	public Double dividir(Operando operandos) {
		// TODO Auto-generated method stub
		return operandos.getValor1() / operandos.getValor2();
	}

	@Override
	public Double porcentaje(Operando operandos) {
		// TODO Auto-generated method stub
		Operando oper = new Operando();
		oper.setValor1(operandos.getValor1());
		oper.setValor2(100.0);
		oper.setValor1(multiplicar(oper));
		oper.setValor2(operandos.getValor2());
		return dividir(oper);
	}
	
}
