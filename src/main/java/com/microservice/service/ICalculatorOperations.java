package com.microservice.service;

import com.microservice.model.Operando;

public interface ICalculatorOperations {
	
	Double sumar(Operando operandos);
	int sumarValoresPositivos(int... numbers);
	Double restar(Operando operandos);
	Double multiplicar(Operando operandos);
	Double dividir(Operando operandos);
	Double porcentaje(Operando operandos);
}
