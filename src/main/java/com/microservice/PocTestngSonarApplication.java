package com.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocTestngSonarApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(PocTestngSonarApplication.class, args);
	}
}
