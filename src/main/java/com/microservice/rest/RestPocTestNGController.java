package com.microservice.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.model.Operando;
import com.microservice.service.ICalculatorOperations;

@RestController
@RequestMapping("/calculadora")
public class RestPocTestNGController {
	
	@Autowired
	private ICalculatorOperations calcOperations;
	
	@PostMapping("/sumar")
	public Double sumar(@RequestBody Operando operandos) {		
		return calcOperations.sumar(operandos);
	}
	
	@PostMapping("/sumarPositivos")
	public int sumarPositivos(@RequestBody int... valores) {	
		return calcOperations.sumarValoresPositivos(valores);
	}
	
	@PostMapping("/restar")
	public Double restar(@RequestBody Operando operandos) {
		return calcOperations.restar(operandos);
	}
	
	@PostMapping("/multiplicar")
	public Double multiplicar(@RequestBody Operando operandos) {
		return calcOperations.multiplicar(operandos);
	}
	
	@PostMapping("/dividir")
	public Double dividir(@RequestBody Operando operandos) {
		return calcOperations.dividir(operandos);
	}
	
	@PostMapping("/porcentaje")
	public Double porcentajeCantidad(@RequestBody Operando operandos) {
		return calcOperations.porcentaje(operandos);
	}

}
