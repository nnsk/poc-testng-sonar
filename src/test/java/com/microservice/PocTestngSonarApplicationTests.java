package com.microservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.microservice.model.Operando;
import com.microservice.rest.RestPocTestNGController;

@SpringBootTest(classes = PocTestngSonarApplication.class)
class PocTestngSonarApplicationTests extends AbstractTestNGSpringContextTests{

	@Autowired
	RestPocTestNGController restController;
	
	@Test (groups = {"solo-suma", "oper-basicas"})
	public void testSumar() {
		Operando operandos = new Operando();
		
		operandos.setValor1(1.0);
		operandos.setValor2(2.0);
		Double valorEsperado = 3.0;
		Assert.assertEquals(restController.sumar(operandos), valorEsperado);
	}
	
	@Test (groups = {"solo-suma"})
	public void testSumaPositivos() {
		int valorEsperado = 10;
		Assert.assertEquals(restController.sumarPositivos(5, 2, 3, -3), valorEsperado);
	}
	
	@Test (groups = {"otras-oper"})
	public void testMultiplicar() {
		Operando operandos = new Operando();
		
		operandos.setValor1(5.0);
		operandos.setValor2(5.0);
		Double valorEsperado = 25.0;
		Assert.assertEquals(restController.multiplicar(operandos), valorEsperado);
	}
	
	@Test (groups = {"otras-oper-2"})
	public void testDividir() {
		Operando operandos = new Operando();
		
		operandos.setValor1(15.0);
		operandos.setValor2(3.0);
		Double valorEsperado = 5.0;
		Assert.assertEquals(restController.dividir(operandos), valorEsperado);
	}
	
	@Test (groups = {"oper-basicas"})
	public void testRestar() {
		Operando operandos = new Operando();
		
		operandos.setValor1(5.0);
		operandos.setValor2(2.0);
		Double valorEsperado = 3.0;
		Assert.assertEquals(restController.restar(operandos), valorEsperado);
	}
	
	@Test (groups = {"oper-no-basicas"})
	public void testPorcentajeCant() {
		Operando operandos = new Operando();
		
		operandos.setValor1(80.0);
		operandos.setValor2(100.0);
		Double valorEsperado = 80.0;
		Assert.assertEquals(restController.porcentajeCantidad(operandos), valorEsperado);
	}
}
